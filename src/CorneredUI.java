package com.evan.chi;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;

/**
 * Created by Administrator on 11/5/2016.
 */
public class CorneredUI {

    GUIFrame parent;
    FullscreenScroller center;
    CornerElement TL;
    CornerElement TR;
    CornerElement BL;
    CornerElement BR;

    public CorneredUI(GUIFrame parent)
    {
        this.parent = parent;
        center = new FullscreenScroller(parent.stage, parent.skin);
        TL = new CornerElement(parent.stage, parent.skin);
        TR = new CornerElement(parent.stage, parent.skin);
        BL = new CornerElement(parent.stage, parent.skin);
        BR = new CornerElement(parent.stage, parent.skin);
    }

    public Label addLabel(String s, int pos)
    {
        Label lbl = new Label(s, parent.skin, "header-label");
        add(lbl, pos);
        return lbl;
    }

    public TextButton addButton(String s, int pos)
    {
        TextButton b = new TextButton(s, parent.skin, "button-english");
        add(b, pos);
        return b;
    }

    public TextButton addButton(String s, int pos, final GUIFrame nextFrame)
    {
        TextButton b = new TextButton(s, parent.skin, "button-english");
        b.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {

                parent.getManager().swap(nextFrame);

                return true;
            }});
        add(b, pos);
        return b;
    }

    public TextButton addButton(String s, int pos, InputListener listener)
    {
        TextButton b = new TextButton(s, parent.skin, "button-english");
        b.addListener(listener);
        add(b, pos);
        return b;
    }



    public void dispose()
    {
        center.dispose();
        BR.dispose();
        BL.dispose();
        TR.dispose();
        TL.dispose();
    }

    public <T extends Actor> Cell<T> add (T actor, int pos)
    {
        Cell<T> cell = null;
        if(pos == Align.center)
        {
            cell = center.add(actor);
        }else if(pos == Align.bottomLeft)
        {
            cell = BL.add(actor, pos);
        }else if(pos == Align.bottomRight)
        {
            cell = BR.add(actor, pos);
        }else if(pos == Align.topRight)
        {
            cell = TR.add(actor, pos);
        }else if(pos == Align.topLeft)
        {
            cell = TL.add(actor, pos);
        }
        return cell;
    }

}
