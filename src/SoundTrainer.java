package com.evan.chi;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Administrator on 11/5/2016.
 */
public class SoundTrainer extends GUIFrame {

    CorneredUI ui;
    ArrayList<String> pinyinList;
    Table cornerTable;
    Label score;
    Label feedback;
    TextButton play;
    TextField answer;
    TextButton check;
    Label help;
    String curPinyin;
    int right;
    int pinyinRight;
    int toneRight;
    int done;
    int setOf;
    Table useTable;
    Table finishedTable;
    Label pinyinScore;
    Label toneScore;
    Label totalScore;
    Label level;

    public void enter()
    {
        ui = new CorneredUI(this);

        cornerTable = new Table();
        useTable = new Table();
        finishedTable = new Table();

        // back
        ui.add(BackButton.getBackButton(this, new MainMenu()), Align.topLeft);

        // score
        score = ActorFactory.getLabel("");
        cornerTable.add(score);
        cornerTable.row();

        // feedback
        feedback = new Label("", skin, "pinyin-meaning");
        cornerTable.add(feedback);
        ui.add(cornerTable, Align.topRight);

        // play
        play = ActorFactory.getButton("Play", new ClickListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                play();
                return true;
            }
        });
        useTable.add(play);
        useTable.row();

        // Textfield
        answer = ActorFactory.getTextField();
        useTable.add(answer);
        useTable.row();

        // Check
        check = ActorFactory.getButton("Enter", new ClickListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                check();
                return true;
            }
        });
        useTable.add(check);
        useTable.row();

        // Help
        help = new Label("Pinyin + Tone ex. bei2\nTones: 1=ā 2=á 3=ǎ 4=à 5=a", skin, "pinyin-meaning");
        useTable.add(help);
        ui.add(useTable, Align.center);


        finishedTable.defaults().padBottom(10);
        pinyinScore = new Label("", skin, "english-meaning");
        finishedTable.add(pinyinScore);
        finishedTable.row();

        toneScore = new Label("", skin, "english-meaning");
        finishedTable.add(toneScore);
        finishedTable.row();

        totalScore = new Label("", skin, "english-meaning");
        finishedTable.add(totalScore);
        finishedTable.row();

        level = new Label("", skin, "header-label");
        finishedTable.add(level);
        finishedTable.row();

        TextButton again = ActorFactory.getButton("Again?", this, new SoundTrainer());
        finishedTable.add(again);

        // Check listener
        ui.center.container.addListener(new InputListener(){
            @Override
            public boolean keyUp (InputEvent event, int keycode) {
                if(keycode == Input.Keys.ENTER)
                    check();

                return true;
            }
        });



        // Init
        pinyinList = new ArrayList<String>(Vocabulary.pinyinList);
        Collections.shuffle(pinyinList, new Random(System.nanoTime()));

        right = 0;
        pinyinRight = 0;
        toneRight = 0;
        done = 0;
        curPinyin = "";
        setOf = 20;

        display(0);
    }

    public void exit()
    {
        ui.dispose();
    }

    private String getRandomPinyin()
    {
        int upper = pinyinList.size();
        Random r = new Random(System.nanoTime());
        int i = r.nextInt(upper);
        return pinyinList.get(i)+Integer.toString(r.nextInt(5)+1);
    }

    private void display(int code)
    {
        if(setOf == done)
        {
            showFinished();
        }else {
            String prevPinyin = new String(curPinyin);
            curPinyin = getRandomPinyin();

            score.setText((setOf - done) + " left");
            if (code == 1) {
                feedback.setText("Good job!");
            } else if (code == 2) {
                feedback.setText("Answer: " + Vocabulary.getPinyin(prevPinyin));
            }
            play();
            answer.setText("");
        }

    }

    private void play()
    {
        Vocabulary.playChinese(Vocabulary.getPinyin(curPinyin));
    }

    private void check()
    {
        if(setOf == done)
            return;
        String s = answer.getText();

        if(!s.matches("[a-zA-Z]+\\d"))
        {
            s = s+"5";
            System.out.println(s);
        }

        if(s.substring(0, s.length()-1).compareTo(curPinyin.substring(0, curPinyin.length()-1)) == 0)
            ++pinyinRight;

        if(s.substring(s.length()-1).compareTo(curPinyin.substring(curPinyin.length()-1)) == 0)
            ++toneRight;

        if(s.compareTo(curPinyin) == 0)
        {
            ++done;
            display(1);
        }else{
            ++done;
            System.out.println(s+"!="+curPinyin);
            display(2);
        }
    }

    private void showFinished()
    {
        useTable.remove();
        ui.add(finishedTable, Align.center);
        ui.TR.container.setVisible(false);

        int total = (int)Math.floor(((double)(pinyinRight+toneRight))/((double)done*2.0)*100.0);
        if(total > 80)
            level.setText("Master Level");
        else if(total > 60)
            level.setText("Expert Level");
        else if(total > 40)
            level.setText("Intermediate Level");
        else if(total > 20)
            level.setText("Novice Level");
        else
            level.setText("Beginner Level");


        pinyinScore.setText("Pinyin Score: "+pinyinRight+" out of "+done);
        toneScore.setText("Tone Score: "+toneRight+" out of "+done);
        totalScore.setText("Score: "+total+" out of "+100);
    }


}