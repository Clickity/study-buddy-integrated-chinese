package com.evan.chi;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import java.util.ArrayList;

public class Main extends ApplicationAdapter {
	Skin skin;
	Stage stage;
    GUIFrameManager fmgr;

    public ArrayList<String> generateLessonList()
    {
        ArrayList<String> list = new ArrayList<String>();
        for(int i=1;i<=20;++i)
        {
            String s1 = "Lesson ";
            s1 += Integer.toString(i);
            String s2 = "";
            s2 += s1;
            s1 += " Dialogue 1";
            s2 += " Dialogue 2";
            list.add(s1);
            list.add(s2);
        }

        return list;
    }

	public BitmapFont generateFont(String language, int size)
	{
		if(language == "english")
		{
            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("verdana.ttf"));
            FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = size;
            return generator.generateFont(parameter);
		}
		if(language == "pinyin")
        {
            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("simsun.ttc"));
            FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = size;
            parameter.characters = Vocabulary.getEnglishCharacterList() + Vocabulary.getPinyinCharacterList();
            return generator.generateFont(parameter);
        }
		if(language == "chinese")
		{
			FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("simsun.ttc"));
			FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
			parameter.size = size;
            parameter.characters = Vocabulary.getChineseCharacterList();
			return generator.generateFont(parameter);
		}

		return new BitmapFont();
	}

	public Texture generateColor(Color color)
    {
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(color);
        pixmap.fill();
        return new Texture(pixmap);
    }






	@Override
	public void create () {

        // Stage <- Stack
        //   ^
        // Skin

        // Init
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		skin = new Skin();
        fmgr = new GUIFrameManager(stage, skin);

		// Skinning
        skin.add("english16", generateFont("english", 16));
        skin.add("english24", generateFont("english", 24));
        skin.add("chinese16", generateFont("chinese", 16));
        skin.add("chinese46", generateFont("chinese", 46));
        skin.add("pinyin24", generateFont("pinyin", 24));
        skin.add("white", generateColor(Color.WHITE));



        Drawable updraw = skin.newDrawable("white", Color.valueOf("#add8e6"));
        updraw.setMinWidth(250);
        updraw.setMinHeight(50);
        TextButton.TextButtonStyle defaultButton = new TextButton.TextButtonStyle();
        defaultButton.up = updraw;
        defaultButton.over = skin.newDrawable("white", Color.valueOf("#8aacb8"));
        defaultButton.fontColor = Color.BLACK;
        defaultButton.font = skin.getFont("english16");
        skin.add("button-english", defaultButton);


        {
            Drawable sdraw = skin.newDrawable("white", Color.valueOf("#D3D3D3"));
            sdraw.setMinWidth(50);
            sdraw.setMinHeight(50);
            TextButton.TextButtonStyle style = new TextButton.TextButtonStyle();
            style.up = sdraw;
            style.over = skin.newDrawable("white", Color.valueOf("#939393"));
            style.fontColor = Color.BLACK;
            style.font = skin.getFont("english16");
            skin.add("button-side", style);
        }

        {
            TextButton.TextButtonStyle checkedButton = new TextButton.TextButtonStyle(defaultButton);
            Drawable cdraw = skin.newDrawable("white", Color.valueOf("#cd0000"));
            cdraw.setMinWidth(125);
            cdraw.setMinHeight(50);
            checkedButton.up = cdraw;
            checkedButton.over = skin.newDrawable("white", Color.valueOf("#8b0000"));
            checkedButton.checked = skin.newDrawable("white", Color.valueOf("#00cd00"));
            checkedButton.checkedOver = skin.newDrawable("white", Color.valueOf("#008b00"));
            skin.add("button-checked", checkedButton);
        }

        {
            Label.LabelStyle style = new Label.LabelStyle();
            style.fontColor = Color.valueOf("#e50000");
            style.font = skin.getFont("english24");
            skin.add("header-label", style);
        }

        {
            Label.LabelStyle style = new Label.LabelStyle();
            style.fontColor = Color.BLACK;
            style.font = skin.getFont("english16");
            skin.add("option-label", style);
        }

        {
            Label.LabelStyle style = new Label.LabelStyle();
            style.fontColor = Color.BLACK;
            style.font = skin.getFont("english24");
            skin.add("english-meaning", style);
        }

        {
            Label.LabelStyle style = new Label.LabelStyle();
            style.fontColor = Color.BLACK;
            style.font = skin.getFont("chinese46");
            skin.add("chinese-meaning", style);
        }

        {
            Label.LabelStyle style = new Label.LabelStyle();
            style.fontColor = Color.BLACK;
            style.font = skin.getFont("pinyin24");
            skin.add("pinyin-meaning", style);
        }

        {
            List.ListStyle listStyle = new List.ListStyle();
            listStyle.font = skin.getFont("english16");
            listStyle.fontColorSelected = Color.BLACK;
            listStyle.selection = skin.newDrawable("white", Color.GRAY);
            listStyle.background = skin.newDrawable("white", Color.LIGHT_GRAY);
            listStyle.fontColorUnselected = Color.BLACK;
            skin.add("default", listStyle);

            ScrollPane.ScrollPaneStyle scrollPaneStyle = new ScrollPane.ScrollPaneStyle();
            skin.add("default", scrollPaneStyle);

            SelectBox.SelectBoxStyle selectBoxStyle = new SelectBox.SelectBoxStyle();
            selectBoxStyle.font = skin.getFont("english16");
            selectBoxStyle.fontColor = Color.BLACK;
            selectBoxStyle.listStyle = listStyle;
            selectBoxStyle.scrollStyle = scrollPaneStyle;
            selectBoxStyle.background = skin.newDrawable("white", Color.valueOf("#add8e6"));
            skin.add("default", selectBoxStyle);
        }

        {
            ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle();
            skin.add("default", style);
        }

        {
            Drawable draw = skin.newDrawable("white", Color.valueOf("#f7ffa5"));
            draw.setMinWidth(250);
            draw.setMinHeight(30);
            TextField.TextFieldStyle style = new TextField.TextFieldStyle();
            style.font = skin.getFont("english16");
            style.fontColor = Color.BLACK;
            style.background = draw;
            style.cursor = skin.newDrawable("white", Color.BLACK);
            skin.add("default", style);
        }

        // Set background
        Texture bgtex = new Texture(Gdx.files.internal("flashcard.png"));
        TextureRegion bgreg = new TextureRegion(bgtex);
        Image bgimg = new Image(bgreg);
        bgimg.setBounds(0,0,stage.getWidth(),stage.getHeight());
        stage.addActor(bgimg);

        ActorFactory.initFactory(skin);


        {
            FileHandle file = Gdx.files.local("SVocab.txt");
            ArrayList<Vocabulary> vocab = Vocabulary.getVocabulary();
            for(int i=0;i<vocab.size();++i)
            {
                Vocabulary v = vocab.get(i);
                ArrayList<String> pinyinList = Vocabulary.getPinyinList(v.pinyin);
                String pine = "";
                for(String s : pinyinList)
                {
                    pine += s;
                }
                String eng = v.english.replaceAll("\\n", "");
                file.writeString(v.chinese+","+v.pinyin+","+pine+","+eng+"\n", true);
            }
        }
        // Start frame
        fmgr.swap(new MainMenu());
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1.f, 0.961f, 0.933f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();
	}

	@Override
	public void resize (int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void dispose () {
		stage.dispose();
		skin.dispose();
	}
}
