package com.evan.chi;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * Created by Administrator on 10/23/2016.
 */
public class BackgroundTrigger {
    public Table container;
    public Stage stage;
    public Skin skin;
    public InputListener listener;

    public BackgroundTrigger(Stage stage, Skin skin)
    {
        this.stage = stage;
        this.skin = skin;

        container = new Table();
        container.setFillParent(true);
        stage.addActor(container);

        Color transparent = new Color(0,0,0,0);
        container.add(new Image(skin.newDrawable("white", transparent))).size(stage.getWidth(), stage.getHeight());

        listener = null;

    }

    public void dispose()
    {
        container.remove();
    }

    public void setListener(InputListener listener)
    {
        if(this.listener != null)
            container.removeListener(this.listener);
        this.listener = listener;
        container.addListener(listener);
    }


}
