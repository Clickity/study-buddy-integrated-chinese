package com.evan.chi;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * Created by Administrator on 10/21/2016.
 */
public abstract class GUIFrame
{
    Stage stage;
    Skin skin;
    GUIFrameManager mgr;

    public void init(GUIFrameManager mgr, Stage stage, Skin skin)
    {
        this.mgr = mgr;
        this.stage = stage;
        this.skin = skin;
        enter();
    }

    public abstract void enter();
    public abstract void exit();

    public GUIFrameManager getManager()
    {
        return mgr;
    }

}
