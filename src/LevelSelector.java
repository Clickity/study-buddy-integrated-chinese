package com.evan.chi;

/**
 * Created by Administrator on 10/25/2016.
 */
public class LevelSelector extends GUIFrame {

    DefaultUI ui;

    public void enter()
    {
        ui = new DefaultUI(this);

        ui.addButton("Level 1", new LessonQuizzes(1));
        ui.addButton("Level 2", new LessonQuizzes(2));
        ui.setBackButton(new MainMenu());

    }

    public void exit()
    {
        ui.dispose();
    }
}

