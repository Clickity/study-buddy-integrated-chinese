package com.evan.chi;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;

/**
 * Created by Administrator on 11/5/2016.
 */
public class ActorFactory {

    public static Skin skin;

    public static void initFactory(Skin skin)
    {
        ActorFactory.skin = skin;
    }



    public static Label getLabel(String s)
    {
        return new Label(s, skin, "header-label");
    }

    public static TextButton getButton(String s)
    {
        return new TextButton(s, skin, "button-english");
    }

    public static TextButton getButton(String s, final GUIFrame curFrame, final GUIFrame nextFrame)
    {
        TextButton b = new TextButton(s, skin, "button-english");
        b.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {

                curFrame.getManager().swap(nextFrame);

                return true;
            }});
        return b;
    }

    public static TextButton getButton(String s, InputListener listener)
    {
        TextButton b = new TextButton(s, skin, "button-english");
        b.addListener(listener);
        return b;
    }

    public static TextField getTextField()
    {
        return new TextField("", skin);
    }
}
