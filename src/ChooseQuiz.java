package com.evan.chi;

/**
 * Created by Administrator on 10/21/2016.
 */
public class ChooseQuiz extends GUIFrame {

    DefaultUI ui;
    int level;
    int lesson;

    public ChooseQuiz(int level, int lesson)
    {
        this.level = level;
        this.lesson = lesson;
    }

    public int getLessonNum(String s)
    {
        return Integer.parseInt(s.replace("Lesson ", ""));
    }

    public void enter()
    {
        ui = new DefaultUI(this);

        ui.addLabel("Lesson "+Integer.toString(lesson));
        ui.addButton("Chinese Word Flashcards", new FlashcardOption(level, lesson));
        //ui.addButton("Chinese Grammer Flashcards(coming soon)");

        ui.setBackButton(new LessonQuizzes(level));

    }

    public void exit()
    {
        ui.dispose();
    }
}
