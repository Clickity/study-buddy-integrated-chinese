package com.evan.chi;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.ArrayList;

/**
 * Created by Administrator on 10/21/2016.
 */
public class GUIFrameManager
{
    ArrayList<GUIFrame> frames;
    Stage stage;
    Skin skin;

    public GUIFrameManager(Stage stage, Skin skin)
    {
        frames = new ArrayList<GUIFrame>();
        this.stage = stage;
        this.skin = skin;
    }


    public void add(GUIFrame frame)
    {
        frames.add(frame);
        frame.init(this, stage, skin);
    }

    public void remove(GUIFrame frame)
    {
        frame.exit();
        frames.remove(frame);
    }

    public void swap(GUIFrame frame)
    {
        pop();
        add(frame);
    }

    public void pop()
    {
        if(frames.size()>0) {
            remove(frames.get(frames.size()-1));
        }
    }

}
