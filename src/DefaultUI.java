package com.evan.chi;


import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class DefaultUI
{
    GUIFrame parent;
    FullscreenScroller root;
    CornerBack back;

    public DefaultUI(GUIFrame parent)
    {
        this.parent = parent;
        root = new FullscreenScroller(parent.stage, parent.skin);
    }

    public void setBackButton(GUIFrame frame)
    {
        back = new CornerBack(parent, frame);
    }

    public void addLabel(String s)
    {
        Label lbl = new Label(s, parent.skin, "header-label");
        add(lbl);
    }

    public void addButton(String s)
    {
        TextButton b = new TextButton(s, parent.skin, "button-english");
        add(b);
    }

    public void addButton(String s, final GUIFrame nextFrame)
    {
        TextButton b = new TextButton(s, parent.skin, "button-english");
        b.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {

                parent.getManager().swap(nextFrame);

                return true;
            }});
        add(b);
    }



    public void dispose()
    {
        if(back != null)
            back.dispose();
        root.dispose();
    }

    public <T extends Actor> Cell<T> add (T actor)
    {
        Cell<T> cell = root.items.add(actor);
        root.items.row();
        return cell;
    }

}
