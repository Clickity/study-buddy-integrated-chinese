package com.evan.chi;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;

/**
 * Created by Administrator on 10/21/2016.
 */
public class CornerBack {

    public CornerElement corner;
    public TextButton back;

    public CornerBack(final GUIFrame curFrame, final GUIFrame backFrame)
    {
        corner = new CornerElement(curFrame.stage, curFrame.skin);
        back = new TextButton("<-", curFrame.skin, "button-side");
        corner.add(back, Align.topLeft);

        back.addListener(new InputListener() {
        public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
            curFrame.getManager().swap(backFrame);
            return true;
        }});
    }

    public void dispose()
    {
        corner.dispose();
    }

}
