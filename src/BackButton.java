package com.evan.chi;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

/**
 * Created by Administrator on 11/5/2016.
 */
public class BackButton {
    static TextButton getBackButton(final GUIFrame curFrame, final GUIFrame backFrame)
    {
        TextButton back = new TextButton("<-", curFrame.skin, "button-side");
        back.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                curFrame.getManager().swap(backFrame);
                return true;
            }});
        return back;
    }
}
