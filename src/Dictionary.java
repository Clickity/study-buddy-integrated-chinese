package com.evan.chi;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;

/**
 * Created by Administrator on 11/20/2016.
 */
public class Dictionary extends GUIFrame {

    FullscreenScroller root;
    TextButton search;
    Table responses;
    ArrayList<Vocabulary> vocab;
    TextField query;
    CornerBack back;


    public void enter()
    {
        //responses = new Table();
        root = new FullscreenScroller(stage, skin);
        query = ActorFactory.getTextField();
        root.add(query);
        search = ActorFactory.getButton("Search", new ClickListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {

                //responses.clearChildren();
                String q = query.getText();
                for(int i=0;i<vocab.size();++i)
                {
                    Vocabulary v = vocab.get(i);
                    ArrayList<String> list = Vocabulary.getPinyinList(v.pinyin);
                    for(int j=0;j<list.size();++j)
                    {
                        if(list.get(j).indexOf(q) != -1)
                        {
                            root.add(getResponseUI(v));
                        }
                    }
                }
                System.out.println(root.scrollPane.getScrollHeight());
                System.out.println(root.scrollPane.getScrollHeight());
                root.container.invalidate();
                root.scrollPane.invalidate();

                return true;
            }
        });
        root.add(search);

        //root.add(responses);
        back = new CornerBack(this, new MainMenu());
        vocab = Vocabulary.getVocabulary();
        System.out.println(root.scrollPane.getScrollHeight());

    }

    public void exit()
    {
        root.dispose();
        back.dispose();
    }

    public Table getResponseUI(Vocabulary v)
    {
        Table table = new Table();
        Label l1 = new Label(v.chinese, skin, "chinese-meaning");
        Label l2 = new Label(v.pinyin+"\n", skin, "pinyin-meaning");
        Label l3 = new Label(v.english, skin, "english-meaning");
        table.add(l1);
        table.row();
        table.add(l2);
        table.row();
        table.add(l3);
        return table;
    }

}
