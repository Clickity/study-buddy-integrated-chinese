package com.evan.chi;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * Created by Administrator on 10/22/2016.
 */
public class CenteredElement {
    public Table container;
    public Stage stage;
    public Skin skin;

    public CenteredElement(Stage stage, Skin skin)
    {
        this.stage = stage;
        this.skin = skin;

        container = new Table();

        // Layout
        container.setFillParent(true);
        stage.addActor(container);
        //container.setSize(stage.getViewport().getScreenWidth(),stage.getViewport().getScreenHeight());
    }

    public void dispose()
    {
        container.remove();
    }

    public void clear()
    {
        container.clearChildren();
    }


    public <T extends Actor> Cell<T> add (T actor) {
        Cell<T> c = container.add(actor);
        //container.setSize(stage.getViewport().getScreenWidth(),stage.getViewport().getScreenHeight());
        return c;
    }

}
