package com.evan.chi;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

/**
 * Created by Administrator on 10/21/2016.
 */
public class FullscreenScroller {
    public Table container;
    public ScrollPane scrollPane;
    public Table items;
    public Stage stage;
    public Skin skin;

    public FullscreenScroller(Stage stage, Skin skin)
    {
        this.stage = stage;
        this.skin = skin;

        items = new Table();
        scrollPane = new ScrollPane(items);
        scrollPane.layout();
        container = new Table();

        // Layout
        container.setFillParent(true);
        container.addActor(scrollPane);
        stage.addActor(container);
        scrollPane.setOrigin(Align.center);
        scrollPane.setWidth(stage.getWidth());
        scrollPane.setHeight(stage.getHeight());
        items.defaults().pad(10,0,10,0);

    }

    public void dispose()
    {
        container.remove();
    }

    public <T extends Actor> Cell<T> add (T actor) {
        Cell<T> cell = container.add(actor).padBottom(10);
        container.row();
        return cell;
    }
}
