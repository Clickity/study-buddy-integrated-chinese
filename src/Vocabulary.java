package com.evan.chi;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.StringBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

import static com.badlogic.gdx.Gdx.files;

/**
 * Created by Administrator on 10/21/2016.
 */
public class Vocabulary {
    String chinese;
    String pinyin;
    String english;


    // lesson->dialogue->vocabulary
    //public static ArrayList<Array<ArrayList<Vocabulary>>> vocab;
    // level->lesson->vocabulary
    public static Array<ArrayList<ArrayList<Vocabulary>>> vocab;
    public static boolean vocabLoaded = false;
    public static String specialChineseChars = "";
    public static String specialPinyinChars = "";
    public static String englishChars = "";
    public static HashMap<String, FileHandle> sounds;
    public static ArrayList<String> pinyinList;

    private static int getTone(String pinyin)
    {
        String[] tones = {"āēīōū", "áéíóú", "ǎěǐǒǔ", "àèìòù"};
        try {
            for(int i=0;i<tones.length;++i)
            {
                tones[i] = new String(tones[i].getBytes(), "UTF-8");
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        for(int i=0;i<4;++i)
        {
            for(int j=0;j<5;++j)
            {
                if(pinyin.indexOf(tones[i].charAt(j)) != -1)
                    return i+1;
            }
        }

        return 5;
    }

    private static String removeSpecialPinyinCharacters(String pinyins)
    {
        String[] aTone = {"āáǎà", "ēéěè", "īíǐì", "ōóǒò", "ūúǔù", "ǖǘǚǜ"};
        String trans = "";
        String npinyins = "";//	ü
        try {
            for(int i=0;i<aTone.length;++i)
            {
                aTone[i] = new String(aTone[i].getBytes("UTF-8"), "UTF-8");
            }
            trans = new String("aeiouv".getBytes("UTF-8"), "UTF-8");
            npinyins = new String(pinyins.getBytes("UTF-8"), "UTF-8");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        for(int i=0;i<pinyins.length();++i)
        {
            for(int j=0;j<6;++j)
            {
                for(int k=0;k<4;++k)
                {
                    if(pinyins.charAt(i) == aTone[j].charAt(k))
                    {
                        npinyins = npinyins.substring(0,i)+trans.charAt(j)+npinyins.substring(i+1);
                    }
                }
            }
        }
        npinyins.replaceAll("ü", "v");
        return npinyins;
    }


    private static String matchPinyin(String pinyins)
    {
        String strippedPinyins = removeSpecialPinyinCharacters(pinyins);
        String match = "";
        for(int i=0;i<pinyinList.size();++i)
        {
            if(strippedPinyins.indexOf(pinyinList.get(i)) == 0 && pinyinList.get(i).length() > match.length())
                match = pinyinList.get(i);
        }

        int tone = getTone(pinyins.substring(0,match.length()));


        return match+Integer.toString(tone);
    }

    private static String processPinyin(String s)
    {
        if(s.indexOf("/") != -1)
        {
            s = s.substring(0, s.indexOf("/"));
        }
        s = s.replaceAll("\\.", "");
        s = s.replaceAll("\\(r\\)", "er");
        s = s.replaceAll("…", "");

        return s;
    }


    public static ArrayList<String> getPinyinList(String pinyins)
    {
        ArrayList<String> pinyin = new ArrayList<String>();
        String[] strs = pinyins.split("\\s|'");


        for(String s : strs) {
            s = processPinyin(s);
            while (s.length() > 0) {
                String match = matchPinyin(s);
                if(match.length() <= 1)
                {
                    System.err.println("Error with string"+s);
                    break;
                }
                pinyin.add(match);
                s = s.substring(match.length() - 1);
            }
        }

        return pinyin;
    }

    public static void playChinese(String pinyin)
    {
        ArrayList<String> pinyinList = getPinyinList(pinyin);
        if(pinyinList.size() == 0)
            return;
        final ArrayList<Music> musicList = new ArrayList<Music>();
        for(int i=0;i<pinyinList.size();++i)
        {
            try {
                FileHandle fileHandle = sounds.get(pinyinList.get(i));
                musicList.add(Gdx.audio.newMusic(fileHandle));
            }catch(IllegalArgumentException e){
                e.printStackTrace();
                System.err.println("Sound playing error");
            }
        }
        for(int i=0;i<pinyinList.size()-1;++i)
        {
            final int index = i+1;
            musicList.get(i).setOnCompletionListener(new Music.OnCompletionListener() {
                @Override
                public void onCompletion(Music music) {
                    musicList.get(index).play();
                }
            });
        }
        musicList.get(0).play();

    }

    public static String getPinyin(String s)
    {
        String base = s.substring(0,s.length()-1);
        int tone = Integer.parseInt(s.substring(s.length()-1));
        if(tone > 4)
            return base;
        --tone;

        if(base.indexOf("v") != -1)
            System.err.println("v found");

        base.replaceAll("v","ü");

        String workingVowels = "aeiou";
        String[] aTone = {"āáǎà", "ēéěè", "īíǐì", "ōóǒò", "ūúǔù"};

        boolean[] hasChar = new boolean[5];
        int ci = 0;
        for(int i=0;i<5;++i)
        {
            hasChar[i] = base.indexOf(workingVowels.charAt(i)) != -1;
        }

        if(hasChar[2]) // i
        {
            ci = 2;
        }
        if(hasChar[1]) // e
        {
            ci = 1;
        }
        if(hasChar[3]) // o
        {
            ci = 3;
        }
        if(hasChar[4]) // u
        {
            ci = 4;
        }
        if(hasChar[0]) // a
        {
            ci = 0;
        }
        if(base.contains("ui"))
        {
            ci = 2;
        }
        if(base.contains("ou")) {
            ci = 3;
        }

        return base.replace(workingVowels.charAt(ci), aTone[ci].charAt(tone));
    }


    public static void loadPinyin()
    {
        pinyinList = new ArrayList<String>();
        sounds = new HashMap<String, FileHandle>();
        FileHandle[] flist = files.internal("audio/").list();
        for(int i=0;i<flist.length;++i)
        {
            String s = "";
            try{
                s = new String(flist[i].name().getBytes("UTF-8"), "UTF-8");
            }catch(Exception e)
            {
                e.printStackTrace();
            }
            String fname = s.replace(".mp3", "");
            String fnamePinyin = fname.replaceAll("[0-9]", "");
            if(i==0 || pinyinList.get(pinyinList.size()-1).compareTo(fnamePinyin) != 0)
                pinyinList.add(fnamePinyin);
            sounds.put(fname, Gdx.files.internal("audio/"+flist[i].name()));
        }
    }

    public static void printHex(String s)
    {
        try {
            byte[] bytes = s.getBytes("UTF-8");

            for(int i=0;i<bytes.length;++i)
            {
                System.err.print(String.format("0x%x    ", bytes[i]));
                if(i%6==0)
                    System.err.println();
            }
            System.err.println();
            System.err.println(s.length());
            System.err.println(bytes.length);
        }catch(Exception e)
        {
            e.printStackTrace();
        }


    }



    public static void loadVocabulary()
    {
        if(!vocabLoaded)
        {
            vocabLoaded = true;
            vocab = new Array<ArrayList<ArrayList<Vocabulary>>>();

            // 2 Levels
            vocab.add(new ArrayList<ArrayList<Vocabulary>>());
            vocab.add(new ArrayList<ArrayList<Vocabulary>>());

            // 20 lessons
            for(int i=0;i<20;++i)
            {
                vocab.get(0).add(new ArrayList<Vocabulary>());
                vocab.get(1).add(new ArrayList<Vocabulary>());
            }


            FileHandle f = Gdx.files.internal("vocabulary.txt");
            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();

            try {
                br = new BufferedReader(new InputStreamReader(
                        f.read(), "UTF-8"));

                char[] buf = new char[512];
                int bytesRead = 0;
                while((bytesRead = br.read(buf, 0, 512)) != -1)
                {
                    sb.append(buf, 0, bytesRead);
                }
            }catch(Exception e){
                e.printStackTrace();
            }

            String fs = sb.toString();

            Scanner scanner = new Scanner(fs);
            //int ln = 1;
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                //System.out.println(line);
                List<String> csl = new ArrayList<String>(Arrays.asList(line.split(",")));

                int level = Integer.parseInt(csl.get(3));
                int lesson = Integer.parseInt(csl.get(4));

                Vocabulary v = new Vocabulary();
                try {
                    v.chinese = new String(csl.get(0).getBytes("UTF-8"), "UTF-8");
                    v.pinyin = new String(csl.get(1).getBytes("UTF-8"), "UTF-8");
                    if (csl.size() == 5) {
                        v.english = "";
                    } else {
                        v.english = parseEnglish(csl.get(5));
                    }
                    //System.err.println(v.pinyin);
                    //printHex(v.pinyin);
                }catch(Exception e)
                {
                    e.printStackTrace();
                }

                vocab.get(level-1).get(lesson-1).add(v);
                //System.out.println(ln++);
            }

            scanner.close();

            ArrayList<Vocabulary> list = getVocabulary();
            for(int i=0;i<list.size();++i)
            {
                specialChineseChars += list.get(i).chinese;
                specialPinyinChars += list.get(i).pinyin;
            }

            specialChineseChars = removeDuplicates(specialChineseChars);
            specialPinyinChars = removeDuplicates(specialPinyinChars);

            loadPinyin();

            StringBuilder eb = new StringBuilder(255);
            for(int i=0;i<255;++i)
            {
                eb.append((char)i);
            }
            try {
                englishChars = new String(eb.toString().getBytes("UTF-8"), "UTF-8");
            }catch(Exception e){
                e.printStackTrace();
            }
        }


    }

    private static String removeDuplicates(String s)
    {
        char[] chars = s.toCharArray();
        Set<Character> charSet = new LinkedHashSet<Character>();
        for (char c : chars) {
            charSet.add(c);
        }

        StringBuilder sb = new StringBuilder();
        for (Character character : charSet) {
            sb.append(character);
        }
        return sb.toString();
    }


    private static String parseEnglish(String s)
    {
        List<String> l = new ArrayList<String>(Arrays.asList(s.substring(1,s.length()-1).split("/")));
        String f = "";
        for(int i=0;i<l.size();++i)
        {
            if(l.get(i).indexOf("CL:") == -1)
            {

                f += "- "+cutLength(l.get(i),30);
                f += "\n";
            }
        }

        if(f.length()>0)
            f = f.substring(0, f.length()-1);

        return f;
    }

    private static String cutLength(String s, int cutInterval)
    {
        String ns = s;
        for(int i=cutInterval;i<ns.length();i+=cutInterval)
        {
            ns = ns.substring(0, i) + "\n" + ns.substring(i, ns.length());
            ++i;
        }
        return ns;
    }


    public static ArrayList<Vocabulary> getVocabulary(int level, int lesson)
    {
        loadVocabulary();

        ArrayList<Vocabulary> newList = new ArrayList<Vocabulary>();
        newList.addAll(vocab.get(level-1).get(lesson-1));
        return newList;
    }

    public static ArrayList<Vocabulary> getVocabulary()
    {
        loadVocabulary();
        ArrayList<Vocabulary> newList = new ArrayList<Vocabulary>();
        for(int i=0;i<2;++i) // lessons
        {
            for(int j=0;j<20;++j) // dialogue
            {
                newList.addAll(vocab.get(i).get(j));
            }
        }
        return newList;
    }

    public static String getChineseCharacterList()
    {
        loadVocabulary();
        return specialChineseChars;
    }

    public static String getPinyinCharacterList()
    {
        loadVocabulary();
        return specialPinyinChars;
    }

    public static String getEnglishCharacterList()
    {
        loadVocabulary();
        return englishChars;
    }

}
