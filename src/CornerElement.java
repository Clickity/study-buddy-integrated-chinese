package com.evan.chi;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

/**
 * Created by Administrator on 10/21/2016.
 */
public class CornerElement {
    public Table container;
    public Stage stage;
    public Skin skin;

    public CornerElement(Stage stage, Skin skin)
    {
        this.stage = stage;
        this.skin = skin;

        container = new Table();

        // Layout
        container.setFillParent(true);
        stage.addActor(container);
    }

    public void dispose()
    {
        container.remove();
    }

    public <T extends Actor> Cell<T> add (T actor, int align) {
        Cell<T> cell = container.add(actor);
        if(align == Align.topLeft)
        {
            cell.left().expandX().top().expandY();
        }else if(align == Align.topRight) {
            cell.right().expandX().top().expandY();
        }else if(align == Align.bottomLeft){
            cell.left().expandX().top().expandY();
        }else if(align == Align.bottomRight) {
            cell.right().expandX().bottom().expandY();
        }
        return cell;
    }

}
