package com.evan.chi;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

/**
 * Created by Administrator on 10/21/2016.
 */
public class FlashcardOption extends GUIFrame {

    FullscreenScroller root;
    CornerBack back;
    int level;
    int lesson;

    public FlashcardOption(int level, int lesson)
    {
        this.lesson = lesson;
        this.level = level;
    }


    public void enter()
    {
        // Dialogue
        // Set amount
        // Front - English, pinyin, chinese
        root = new FullscreenScroller(stage, skin);

        /*
        final TextButton diag1 = new TextButton("Show Dialogue 1", skin, "button-checked");
        final TextButton diag2 = new TextButton("Show Dialogue 2", skin, "button-checked");
        diag1.setChecked(true);
        diag2.setChecked(true);*/


        Object[] setEntries = {"All at once", "Set of 5", "Targeted Recall"};
        Label setLabel = new Label("Mode:", skin, "option-label");
        final SelectBox setType = new SelectBox(skin);
        setType.setItems(setEntries);

        Object[] frontEntries = {"Chinese Character", "Pinyin", "English Translation"};
        Label frontLabel = new Label("Clue:", skin, "option-label");
        final SelectBox frontType = new SelectBox(skin);
        frontType.setItems(frontEntries);

        TextButton start = new TextButton("Start", skin, "button-english");

        //root.items.add(diag1);
        //root.items.add(diag2);
        //root.items.row();
        root.items.add(setLabel);
        root.items.add(setType);
        root.items.row();
        root.items.add(frontLabel);
        root.items.add(frontType);
        root.items.row();
        root.items.add(start).colspan(2);


        start.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {

                getManager().swap(new Flashcards(level, lesson, setType.getSelectedIndex(), frontType.getSelectedIndex()));
                return true;
            }});

        back = new CornerBack(this, new ChooseQuiz(level, lesson));

    }

    public void exit()
    {
        root.dispose();
        back.dispose();
    }
}