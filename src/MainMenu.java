package com.evan.chi;

/**
 * Created by Administrator on 10/21/2016.
 */
public class MainMenu extends GUIFrame {

    DefaultUI ui;


    public void enter()
    {
        ui = new DefaultUI(this);

        ui.addLabel("Integrated Chinese Study Tool");
        ui.addButton("Flashcards", new LevelSelector());
        ui.addButton("Sound Trainer", new SoundTrainer());
        ui.addButton("Dictionary", new Dictionary());
    }

    public void exit()
    {
        ui.dispose();
    }

}
