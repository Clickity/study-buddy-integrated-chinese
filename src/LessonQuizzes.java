package com.evan.chi;

import java.util.ArrayList;

/**
 * Created by Administrator on 10/21/2016.
 */
public class LessonQuizzes extends GUIFrame {

    DefaultUI ui;
    int level;

    public LessonQuizzes(int level)
    {
        this.level = level;
    }

    public ArrayList<String> generateLessons()
    {
        ArrayList<String> list = new ArrayList<String>();
        for(int i=1;i<=20;++i)
        {
            list.add("Lesson "+Integer.toString(i));
        }
        return list;
    }

    public void enter()
    {
        ui = new DefaultUI(this);

        ui.setBackButton(new MainMenu());
        ArrayList<String> list = generateLessons();
        for(int i=0;i<list.size();++i)
        {
            ui.addButton(list.get(i), new ChooseQuiz(level, i+1));
        }

    }

    public void exit()
    {
        ui.dispose();
    }
}
