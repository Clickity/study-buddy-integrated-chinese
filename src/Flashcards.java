package com.evan.chi;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Administrator on 10/21/2016.
 */
public class Flashcards extends GUIFrame {

    BackgroundTrigger trigger;
    CornerBack back;
    CornerElement next;
    CornerElement recall;
    CornerElement feedback;
    Table recallTable;
    CenteredElement center;
    Label feedbacklbl;

    int level;
    int lesson;
    int displayType;
    int frontType;
    ArrayList<Vocabulary> vocabList;
    ArrayList<Vocabulary> setList;
    int index;
    int setIndex;

    public Flashcards(int level, int lesson, int displayType, int frontType)
    {
        this.level = level;
        this.lesson = lesson;
        this.displayType = displayType;
        this.frontType = frontType;
    }


    public void enter()
    {
        // Init UI
        center = new CenteredElement(stage, skin);
        trigger = new BackgroundTrigger(stage, skin);
        back = new CornerBack(this, new FlashcardOption(level, lesson));
        recall = new CornerElement(stage, skin);
        recallTable = new Table();

        next = new CornerElement(stage, skin);
        TextButton nextButton = new TextButton("Next", skin, "button-side");
        next.add(nextButton, Align.bottomRight);
        nextButton.addListener(new ClickListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                display(0);
                return true;
            }
        });

        TextButton remeberedButton = new TextButton("Remembered", skin, "button-side");
        recallTable.add(remeberedButton).padRight(10);
        remeberedButton.addListener(new ClickListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                display(1);
                return true;
            }
        });

        TextButton forgotButton = new TextButton("Forgot", skin, "button-side");
        recallTable.add(forgotButton).padRight(10);;
        forgotButton.addListener(new ClickListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                display(2);
                return true;
            }
        });
        recall.add(recallTable, Align.bottomRight);

        feedback = new CornerElement(stage, skin);
        feedbacklbl = new Label("", skin, "option-label");
        feedback.add(feedbacklbl, Align.topRight);

        initVocab();
        display(0);

    }

    public void exit()
    {
        trigger.dispose();
        back.dispose();
        next.dispose();
        center.dispose();
        recall.dispose();
        feedback.dispose();
    }

    public void initVocab()
    {
        vocabList = Vocabulary.getVocabulary(level, lesson);

        Collections.shuffle(vocabList, new Random(System.nanoTime()));
        index = 0;
        setIndex = 0;
        makeSet(0);
    }

    public void makeSet(int si)
    {
        setList = new ArrayList<Vocabulary>();
        for(int i=si;i<vocabList.size();++i)
        {
            setList.add(vocabList.get(i));

            if(i-si == 4)
                break;
        }
        Collections.shuffle(setList, new Random(System.nanoTime()));
    }


    public void display(int v)
    {
        // Displays:
        // card - front,back
        // next set
        // finished

        if(displayType == 0) // All cards
        {
            if(index < vocabList.size()) // display card
            {
                showCard(vocabList.get(index), false);
                ++index;
            }else{ // display finished
                showFinished();
            }
        }else if(displayType == 1) // Sets
        {
            if(index < vocabList.size())
            {
                if(setIndex < setList.size())
                {
                    showCard(setList.get(setIndex), false);
                    ++setIndex;
                }else{
                    showSetFinished();
                }
            }else{
                showFinished();
            }
        }else if(displayType == 2)
        {
            if(v == 2) {
                ++index;
            }else if(v == 1)
            {
                vocabList.remove(index);
            }
            if(index < vocabList.size()) // display card
            {
                showCard(vocabList.get(index), false);
            }else if(vocabList.size() == 0){ // display finished
                showFinished();
            }else{
                Collections.shuffle(setList, new Random(System.nanoTime()));
                index = 0;
                showCard(vocabList.get(index), false);
            }
        }
    }

    public void showCard(final Vocabulary v, final boolean flipped)
    {
        if(displayType == 2)
        {
            recall.container.setVisible(true);
            next.container.setVisible(false);
            feedback.container.setVisible(true);
            feedbacklbl.setText(vocabList.size()+" left");
        }else{
            recall.container.setVisible(false);
            next.container.setVisible(true);
            feedback.container.setVisible(false);
        }

        trigger.container.setVisible(true);

        // Setup strings
        Label l1, l2, l3;
        if(frontType == 0)
        {
            if(flipped)
                Vocabulary.playChinese(v.pinyin);
            l1 = new Label(v.chinese, skin, "chinese-meaning");
            l2 = new Label(v.english, skin, "english-meaning");
            l3 = new Label(v.pinyin, skin, "pinyin-meaning");
        }else if(frontType == 1)
        {
            if(!flipped)
                Vocabulary.playChinese(v.pinyin);
            l1 = new Label(v.pinyin, skin, "pinyin-meaning");
            l2 = new Label(v.chinese, skin, "chinese-meaning");
            l3 = new Label(v.english, skin, "english-meaning");
        }else
        {
            if(flipped)
                Vocabulary.playChinese(v.pinyin);
            l1 = new Label(v.english, skin, "english-meaning");
            l2 = new Label(v.chinese, skin, "chinese-meaning");
            l3 = new Label(v.pinyin, skin, "pinyin-meaning");
        }

        center.clear();
        if(!flipped)
        {
            center.add(l1);
        }else{
            center.add(l2).padBottom(15);
            center.container.row();
            center.add(l3);
        }

        trigger.setListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {

                showCard(v, !flipped);
                return true;
            }});

    }

    public void showFinished()
    {
        next.container.setVisible(false);
        trigger.container.setVisible(false);
        recall.container.setVisible(false);
        feedback.container.setVisible(false);

        TextButton fin = new TextButton("Again?", skin,"button-english");
        center.clear();
        center.add(fin);
        fin.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {

                initVocab();
                display(0);

                return true;
            }});
    }

    public void showSetFinished()
    {
        next.container.setVisible(false);
        trigger.container.setVisible(false);

        TextButton redoSet = new TextButton("Redo Set", skin,"button-english");
        TextButton nextSet = new TextButton("Next Set", skin,"button-english");

        center.clear();
        center.add(redoSet).padBottom(10);
        center.container.row();
        center.add(nextSet);

        redoSet.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {

                setIndex = 0;
                makeSet(index);
                display(0);

                return true;
            }});

        nextSet.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {

                index += setList.size();
                setIndex = 0;
                makeSet(index);
                display(0);

                return true;
            }});
    }




}